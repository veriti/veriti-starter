'use strict';

var path = require('path');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  devtool: '#cheap-source-map',
  context: __dirname,
  entry: [
   './source/entry.js'
  ],
  output: {
    path: path.join(__dirname, 'public'),
    filename: 'js/app.js',
    publicPath: '/'
  },
  module: {
    loaders: [
      {
        test: /\.js?$/,
        loader: 'babel',
        include: path.join(__dirname, 'source'),
      },
      {
        test: /\.css$/i,
        loader: ExtractTextPlugin.extract(
          'style-loader',
          'css-loader?modules&localIdentName=[name]__[local]--[hash:base64:5]'
        ),
      },
    ]
  },
  plugins: [
    new ExtractTextPlugin('css/[name].css'),
    new HtmlWebpackPlugin({
      template: 'source/index.html',
      filename: 'index.html'
    })
  ],
  devServer: {
    inline: true,
    port: 3000,
    host: '0.0.0.0',
    contentBase: 'public/'
  }
};
